
Login Ticket API -
Facilities for other modules to let people log in as Drupal user
as long as their login ticket is valid.


SHORT DESCRIPTION
-----------------
This is a pure API module, providing functions for generating login tickets.

A login ticket consists of an automatically generated pass code,
an expiration date, and a user who may log in with the pass code.
This module provides functions for generating, retrieving and deleting
login tickets, as well as checking validness of a given pass code and,
given a valid code (while not requiring username and password),
logging the concerned user in.

The Login Ticket API was designed to act as backend for modules like
Temporary Invitation and Site Pass, with less (but still some) parts of the API
being suitable for the Invite module as well.

For the API documentation, have a look at the module file or run phpdoc over it
to get a fancier version of the docs.


AUTHOR/MAINTAINER
-----------------
Jakob Petsovits <jpetso at gmx DOT at>


CREDITS
-------
Some code in Login Ticket was taken from the Site Pass module for Drupal 5.x
which was written by D R Pratten <http://www.davidpratten.com>.
